import React from 'react';
import { Store, StoreEvents } from '../store.jsx';

class SidebarSeriesItem extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      className: props.hasOwnProperty('className') ? props.className : '',
      label: props.label
    };
  }
  
  render() {
    return (
      <li className={this.props.className}>
        <a href="#" onClick={(e) => this.handleClick(e)} onKeyDown={(e) => this.handleKeyDown(e)} className="sh-item-link" tabIndex="0">
          <span>{this.props.label}</span>
        </a>
      </li>
    );
  }
  
  handleClick(e) {
    e.preventDefault();
    if (this.props.hasOwnProperty('onClick')) {
      this.props.onClick(this.props.id);
    }
  }

  handleKeyDown(e) {
    if (-1 !== [32, 13].indexOf(e.keyCode)) {
      e.preventDefault();
      e.stopPropagation();
      this.handleClick(e);
    }
  }
}

export class Sidebar extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      filterId: null,
      filterSeries: null,
    };
  }

  render() {
    const allItem = <SidebarSeriesItem label="Wszystkie" id={null} onClick={(e) => this.handleSeriesClick(e, null)} className={'sh-series-item' + (this.state.filterId === null ? ' is-active' : '')} />;
    const items = [...Store.get().playlistSeries]
      .sort((itemA, itemB) => itemA.id < itemB.id)
      .filter((item) => null === this.state.filterSeries || null === item.id || null !== item.name.toLowerCase().match(new RegExp(this.state.filterSeries.split(' ').join("|").toLowerCase())))
      .slice(0, 10)
      .map((item) => <SidebarSeriesItem label={item.name} id={item.id} onClick={(e) => this.handleSeriesClick(e, item.id)} className={'sh-series-item' + (this.state.filterId === item.id ? ' is-active' : '')} />);

    items.unshift(allItem);

    return (
      <section className="sh-panel sh-player-sidebar-left">
        <header className="sh-panel-header">
          <h3 className="sh-panel-title">SERIE</h3>
        </header>
        <div className="sh-panel-body">
          <input type="text" className="form-control sh-control-filter-series" placeholder="Szukaj serii..." onKeyUp={(e) => this.handleFilterKeyUp(e)} />
          <ul className="sh-series-list">
            {items}
          </ul>
        </div>
      </section>
    )
  }
  
  handleFilterKeyUp(e) {
    e.preventDefault();
    const series = e.target.value;
    clearTimeout(this._timeoutSeries);
    this._timeoutSeries = setTimeout(() => this.setState({ filterSeries: series }), 150);
  }

  handleSeriesClick(e, id) {
    this.setState({
      filterId: id
    });

    StoreEvents.emit('playlistFilterSeries', id);
  }
}
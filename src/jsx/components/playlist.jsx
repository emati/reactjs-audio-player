import React from 'react';
import { PODCAST_PLACEHOLDER_1X1 } from '../config.jsx';
import { secondsToTime } from '../utils.jsx';
import { Store, StoreEvents } from '../store.jsx';
import { AudioState } from '../state.jsx';
import { IconPlay, IconPause } from './icons.jsx';

class PlaylistItem extends React.Component {
  render() {
    const classes = ['sh-playlist-item', 'is-' + this.props.state.toLowerCase()];
    !this.props.active ? null : classes.push('active');
    
    return (
      <li className={classes.join(' ')}>
        <figure className="sh-podcast-disc" onClick={(e) => this.handleClick(e)} onKeyDown={(e) => this.handleKeyDown(e)} title={this.props.item.title} tabIndex="0">
          <div className="sh-podcast-image">
            { this.props.state === 'LOADING' ? <div className="sh-podcast-loader"><div></div></div> : '' }
            <img src={ true && (this.props.item.thumbnail_1x1 || PODCAST_PLACEHOLDER_1X1) || PODCAST_PLACEHOLDER_1X1 } className="sh-podcast-thumb-4x3" alt={this.props.item.title + ' - ' + this.props.item.author} />
            <div className="sh-podcast-state">
              <IconPause className="icon-pause" />
              <IconPlay className="icon-play" />
            </div>
          </div>
          <figcaption className="sh-podcast-details">
            <span className="sh-podcast-title">{this.props.item.title}</span>
            <span className="sh-podcast-author">{this.props.item.author}</span>
            <span className="sh-podcast-duration">{secondsToTime(this.props.item.duration)}</span>
          </figcaption>
        </figure>
      </li>
    );
  }
  
  handleClick(e) {
    e.preventDefault();
    if (!this.props.active) {
      AudioState.stop();
      AudioState.load(this.props.index);
      AudioState.play();
    } else if(this.props.state === 'PLAYING') {
      AudioState.pause();
    } else if(-1 !== ['PAUSE', 'LOADED'].indexOf(this.props.state)) {
      AudioState.play();
    }
  }

  handleKeyDown(e) {
    if (-1 !== [32, 13].indexOf(e.keyCode)) {
      e.preventDefault();
      e.stopPropagation();
      this.handleClick(e);
    }
  }
}

class PlaylistItems extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      filter: '',
      seriesFilter: null,
      state: 'IDLE',
      itemsPerPage: 15,
      page: 1,
      isLastPage: false,
      activeIndex: Store.get().playlistIndex,
    };
  }

  componentDidMount() {
    StoreEvents.on('loading', (item, index) => this.setState({ state: 'LOADING', activeIndex: index }));
    StoreEvents.on('loaded', (item) => this.setState({ state: 'LOADED' }));
    StoreEvents.on('play', (item) => this.setState({ state: 'PLAYING' }));
    StoreEvents.on('pause', (item) => this.setState({ state: 'PAUSE' }));
    StoreEvents.on('idle', (item, index) => this.setState({ state: 'IDLE', activeIndex: -1 }));
    StoreEvents.on('playlistSearch', (phrase) => this.setState({ filter: phrase, page: 1 }));
    StoreEvents.on('playlistFilterSeries', (id) => this.setState({ seriesFilter: id, page: 1 }));
  }
  
  render() {
    this.props.filteredItems = [...Store.get().playlist]
      .filter((item) => '' === this.state.filter || null !== item.title.toLowerCase().match(new RegExp(this.state.filter.split(' ').join("|").toLowerCase())))
      .filter((item) => null === this.state.seriesFilter || item.series === this.state.seriesFilter)
      .sort((itemA, itemB) => itemA.index > itemB.index);
    const rows = this.props.filteredItems.map((item, index) => <PlaylistItem key={item.index} index={item.index} active={this.state.activeIndex === item.index} item={item} state={this.state.activeIndex === item.index ? this.state.state : 'IDLE'} />)
      .slice((this.state.page - 1) * this.state.itemsPerPage, this.state.page * this.state.itemsPerPage);
    
    return (
      <div className="sh-playlist-items">
        <ol className="sh-player-playlist">
          {rows}
        </ol>
        <div className="sh-playlist-pager">
          <div className="row">
            <div className="col-xs-6 col-sm-4 col-md-2">
              { this.state.page > 1 && <button type="button" onClick={(e) => this.prevPage(e)} className="sh-btn-prev">Wstecz</button> }
            </div>
            <div className="col-xs-6 col-sm-4 col-md-2 pull-right text-right">
              { this.state.page < this.getMaxPage() && <button type="button" onClick={(e) => this.nextPage(e)} className="sh-btn-next">Dalej</button> }
            </div>
            <div className="col-xs-12 col-sm-4 col-md-8 text-center">Strona: {this.state.page + ' / ' + this.getMaxPage()}</div>
          </div>
        </div>
      </div>
    )
  }

  prevPage(e) {
    e.preventDefault();
    this.setState({ page: Math.max(1, this.state.page - 1)});
  }

  nextPage(e) {
    e.preventDefault();
    this.setState({ page: Math.min(this.getMaxPage(), this.state.page + 1)});
  }

  getMaxPage() {
    return Math.ceil(this.props.filteredItems.length / this.state.itemsPerPage);
  }
}

export class Playlist extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      headline: 'Najnowsze Wykłady',
    };
  }

  render() {

    return (
      <section className="sh-playlist">
        <header className="sh-playlist-header">
          <div className="row">
            <div className="col-sm-8">
              <h2 className="sh-playlist-title">{this.state.headline}</h2>
            </div>
            <div className="col-sm-4">
              <input type="text" placeholder="Szukaj wykładu..." className="form-control sh-control-search" onKeyUp={(e) => this.handleFilter(e)} />
            </div>
          </div>
        </header>
        <PlaylistItems />
      </section>
    );
  }

  handleFilter(e) {
    e.preventDefault();
    const filterVal = e.target.value;
    clearTimeout(this._filterTimeout);
    this._filterTimeout = setTimeout(() => {
      StoreEvents.emit('playlistSearch', filterVal);
    }, 200);
  }
}
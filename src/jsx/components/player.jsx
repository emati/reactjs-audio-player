import React from 'react';
import ReactDOM from 'react-dom';
import InputSlider from 'react-input-slider';
import Sticky from 'react-stickynode';
import { Sidebar } from './sidebar.jsx';
import { Store, Audio, Source, Container, StoreEvents } from '../store.jsx';
import { AudioState } from '../state.jsx';
import { IconPlay, IconPause, IconPrev, IconNext, IconDownload } from './icons.jsx';
import { Playlist, PlaylistLiItem } from './playlist.jsx';
import { secondsToTime } from '../utils.jsx';

export class AudioTimer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { timer: '-:- / -:-' };
  }

  componentDidMount() {
    StoreEvents.on('idle', () => this.setState({ timer: '-:- / -:-' }));
    StoreEvents.on('loading', () => this.setState({ timer: '-:- / -:-' }));
    StoreEvents.on('loaded', () => this.setState({ timer: '00:00 / ' + secondsToTime(Audio.duration) }));
    Audio.addEventListener('timeupdate', () => this.setState({ timer: secondsToTime(Audio.currentTime) + ' / ' + secondsToTime(Audio.duration) }), false);
  }
  
  render() {
    return (
      <div className="sh-player-timer">{this.state.timer}</div>
    );
  }
}

export class ControlsSeek extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      seek: 0,
      seekMax: 0,
      isSeeking: false
    };
  }
  
  componentDidMount() {
    StoreEvents.on('loading', () => this.setState({ isSeeking: true }));
    StoreEvents.on('loaded', () => this.setState({ seekMax: Audio.duration, isSeeking: false }));
    Audio.addEventListener('timeupdate', () => this.setState({ seek: Audio.currentTime }), false);
    Audio.addEventListener('seeked', () => this.setState({ isSeeking: false }), false);
  }
  
  render() {
    return (
      <InputSlider
        className={ 'sh-control-seek slider-x ' + (this.state.isSeeking ? 'seeking' : '') }
        axis="x"
        xmax={this.state.seekMax}
        step={1}
        x={this.state.seek}
        onChange={(pos) => this.handleSeek(pos)}
      />
    )
  }
  
  handleSeek(pos) {
    this.setState({
      seek: pos.x,
      isSeeking: true
    });
    Audio.currentTime = pos.x;
  }
}

export class Controls extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      audioLength: 0,
      volume: 75,
      downloadURL: '#',
      audioState: AudioState.getMachineState()
    };
  }
  
  componentDidMount() {
    StoreEvents.on('loading', (item) => this.setState({ downloadURL: item.url }));
    StoreEvents.on('idle', () => this.setState({ audioState: 'IDLE' }));
    StoreEvents.on('play', () => this.setState({ audioState: 'PLAYING' }));
    StoreEvents.on('pause', () => this.setState({ audioState: 'PAUSE' }));
  }
  
  render() {
    return (
      <Sticky top='.navbar-default' bottomBoundary='#main-footer'>
        <div className="sh-player-controls">
          <div className="container">
            <div className="col-lg-2 col-md-3">
              <div className="sh-control-buttons">
                <a href="#" onClick={(e) => this.handlePrev(e)} className={Store.get().playlistIndex === 0 ? 'disabled' : ''} className="sh-control-prev">
                  <IconPrev />
                </a>
                { 
                  this.state.audioState === 'PLAYING' ? 
                  (
                    <a href="#" onClick={(e) => this.handlePlayPause(e)} className="sh-control-pause">
                      <IconPause />
                    </a>
                  ) : (
                    <a href="#" onClick={(e) => this.handlePlayPause(e)} className="sh-control-play">
                      <IconPlay />
                    </a>
                  )
                }
                <a href="#" onClick={(e) => this.handleNext(e)} className={Store.get().playlistIndex === ( Store.get().playlist.length - 1 ) ? 'disabled' : ''} className="sh-control-next">
                  <IconNext />
                </a>
              </div>
            </div>
            <div className="col-lg-8 col-sm-9 col-md-6">
              <div className="sh-player-seeker">
                <ControlsSeek />
                <AudioTimer />
              </div>
            </div>
            <div className="col-lg-2 col-sm-3 col-md-3">
              <div className="sh-player-controls2">
                <InputSlider
                  className="sh-control-volume slider-x"
                  axis="x"
                  xmin={0}
                  xmax={100}
                  step={1}
                  x={this.state.volume}
                  onChange={(pos) => this.handleVolume(pos)}
                />
                <IconDownload linkhref={this.state.downloadURL} />
              </div>
            </div>
          </div>
        </div>
      </Sticky>
    );
  }
  
  handlePlayPause(e) {
    e.preventDefault();
    switch (AudioState.getMachineState()) {
      case 'IDLE':
      case 'PAUSED':
      case 'LOADED':
        AudioState.play();
        break;
      case 'PLAYING':
        AudioState.pause();
        break;
    }
  }
  
  handleNext(e) {
    e.preventDefault();
    const store = Store.get();
    AudioState.stop();
    AudioState.load(Math.min((store.playlist.length - 1), (store.playlistIndex + 1)));
    AudioState.play();
  }
  
  handlePrev(e) {
    e.preventDefault();
    const store = Store.get();
    AudioState.stop();
    AudioState.load(Math.max(0, (store.playlistIndex - 1)));
    AudioState.play();
  }
  
  handleVolume(pos) {
    this.setState({
      volume: pos.x
    });
    Audio.volume = pos.x / 100;
  }
}

export class Header extends React.Component {
  render() {
    return (
      <div className="sh-player-jumbo">
        <Controls />
      </div>
    );
  }
}

export class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false, errorInfo: null };
  }

  componentDidCatch(error, info) {
    // Display fallback UI
    this.setState({ hasError: true, errorInfo: info });
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return (
        <div>
          <h1>Something went wrong.</h1>
          <pre>{this.state.errorInfo}</pre>
        </div>
      );
    }
    return this.props.children;
  }
}

export class Player extends React.Component {
  render() {
    return (
      <ErrorBoundary>
        <Header />
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-md-4 hidden-xs hidden-sm">
              <Sidebar />
            </div>
            <div className="col-lg-9 col-md-8">
              <Playlist />
            </div>
          </div>
        </div>
      </ErrorBoundary>
    );
  }
}

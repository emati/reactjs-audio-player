import * as React from 'react';

export class IconPlay extends React.Component {
  render() {
    return (
      <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 36 36" aria-labelledby="title" {...this.props}>
        <title id="title">Wznów/Zacznij odtwarzanie</title>
        <circle cx="18" cy="18" r="17.03092" style={{fill: 'none', 'strokeInecap': 'round', 'strokeLinejoin': 'round', 'strokeWidth': 1.93815935}}/>
        <path d="m 26.095173,17.919635 c -4.308382,2.48745 -8.616764,4.97489 -12.925146,7.46234 0,-4.97489 0,-9.94978 0,-14.92468 4.308382,2.48745 8.616764,4.9749 12.925146,7.46234 z"/>
      </svg>
    )
  }
};

export class IconPause extends React.Component {
  render() {
    return (
      <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 29 29" aria-labelledby="title" {...this.props}>
        <title id="title">Zapauzuj odtwarzanie</title>
        <path d="M0 0h9v29H0zm14 0h9v29h-9z" transform="translate(3, 0)"/>
      </svg>
    )
  }
};

export class IconPrev extends React.Component {
  render() {
    return (
      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="12" viewBox="0 0 39 29" aria-labelledby="title" {...this.props}>
        <title id="title">Odtwórz poprzedni wykład</title>
        <path d="M39 28.524L22 14.175 39-.175zm-17 0L5 14.175 22-.175zM5 0v29H0V0z"/>
      </svg>
    )
  }
};

export class IconNext extends React.Component {
  render() {
    return (
      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="12" viewBox="0 0 39 29" aria-labelledby="title" {...this.props}>
        <title id="title">Odtwórz następny wykład</title>
        <path d="M0 28.524l17-14.349L0-.175zm17 0l17-14.349L17-.175zM34 0v29h5V0z"/>
      </svg>
    )
  }
};

export class IconDownload extends React.Component {
  render() {
    return (
      <a href={this.props.linkhref} {...this.props}>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" width="27" height="24" aria-labelledby="title">
          <title id="title">Pobierz wykład w MP3</title>  
          <path d="M216 0h80c13.3 0 24 10.7 24 24v168h87.7c17.8 0 26.7 21.5 14.1 34.1L269.7 378.3c-7.5 7.5-19.8 7.5-27.3 0L90.1 226.1c-12.6-12.6-3.7-34.1 14.1-34.1H192V24c0-13.3 10.7-24 24-24zm296 376v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V376c0-13.3 10.7-24 24-24h146.7l49 49c20.1 20.1 52.5 20.1 72.6 0l49-49H488c13.3 0 24 10.7 24 24zm-124 88c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20zm64 0c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20z"></path>
        </svg>
      </a>
    )
  }
};
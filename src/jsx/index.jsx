import 'promise-polyfill';
import 'whatwg-fetch';

import React from 'react';
import ReactDOM from 'react-dom';
import { Player } from './components/player.jsx';
import { Store, Container } from './store.jsx';
import { AudioState } from './state.jsx';
    
fetch(window.shPlayerConfig.ajaxUrl + '?action=sh_player_data').then((res) => res.json())
  .then((resData) => {
    Store.get().set({
      playlist: resData.playlist,
      playlistSeries: resData.series,
    });

    AudioState.ready();
    ReactDOM.render(<Player />, Container, () => {
      AudioState.load(0);
    });
  });
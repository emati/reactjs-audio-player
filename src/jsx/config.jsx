export const PODCAST_PLACEHOLDER_1X1 = window.shPlayerConfig.assetsUrl + '/img/podcast-placeholder-1x1.jpg';
export const PODCAST_PLACEHOLDER_4X3 = window.shPlayerConfig.assetsUrl + '/img/podcast-placeholder-4x3.jpg';
export const PODCAST_PLACEHOLDER_16X9 = window.shPlayerConfig.assetsUrl + '/img/podcast-placeholder-16x9.jpg';
export const PODCAST_ITEM_LOADING_PATTERN = window.shPlayerConfig.assetsUrl + '/img/black-twill.png';
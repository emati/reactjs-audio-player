export const secondsToTime = (seconds) => {
  const time = Math.round(seconds);
  const minutes = Math.floor(time / 60);
  return stringPad(String(minutes), '0', 2) + ':' + stringPad(String(time - minutes * 60), '0', 2);
}

export const stringPad = (string, fill, length) => {
  return Array(Math.abs(length-String(string).length+1)).join(fill)+string;
}
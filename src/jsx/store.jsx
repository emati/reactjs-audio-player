var Freezer = require('freezer');

let audio = document.createElement('audio');
let source = document.createElement('source');
let container = document.getElementById('sh-player-wrapper');

audio.id = 'sh-player-audio';
audio.appendChild(source);
container.parentNode.insertBefore(audio, container.nextSibling);

export const Store = new Freezer({
  playlistFilter: '',
  playlist: [],
  currentAudio: null,
  playlistIndex: null,
  playlistSeries: [],
});

export const Audio = document.getElementById('sh-player-audio');
export const Source = document.querySelector('#sh-player-audio source');
export const Container = container;
export const StoreEvents = Store.getEventHub();
var Stately = require('stately');
import { Store, Audio, Source, StoreEvents } from './store.jsx';

const loadAudio = (index) => {
  const store = Store.get();
  if (store.playlist[index]) {
    store.set({
      playlistIndex: index,
      currentAudio: store.playlist[index]
    });
  } else {
    index = null;
  }

  return index !== null ? ['LOADING', index] : ['IDLE', index];
}

export const AudioState = Stately.machine({
  'PRELOADING': {
    ready: 'IDLE'
  },
  'IDLE': {
    onEnter: () => {
      const store = Store.get();
      store.set({
        playlistIndex: null,
        currentAudio: null
      });

      StoreEvents.emit('idle');
    },
    load: loadAudio
  },
  'PLAYING': {
    onEnter: () => {
      const store = Store.get();
      Audio.play();
      StoreEvents.emit('play', store.currentAudio, store.playlistIndex);
    },
    pause: 'PAUSED',
    stop: 'IDLE'
  },
  'LOADING': {
    onEnter: () => {
      const store = Store.get();
      Source.src = store.currentAudio.url;
      Audio.load();

      const emitLoad = () => {
        Audio.removeEventListener('canplay', emitLoad, false);
        AudioState.loaded();
      };

      Audio.addEventListener('canplay', emitLoad, false);
      StoreEvents.emit('loading', store.currentAudio, store.playlistIndex);
    },
    load: loadAudio,
    play: () => {
      // Auto play
      const emitLoad = () => {
        Audio.removeEventListener('canplay', emitLoad, false);
        AudioState.play();
      };

      Audio.addEventListener('canplay', emitLoad, false);
    },
    loaded: () => 'LOADED'
  },
  'LOADED': {
    onEnter: () => {
      const store = Store.get();
      StoreEvents.emit('loaded', store.currentAudio, store.playlistIndex);
    },
    play: () => {
      const store = Store.get();
      if (!store.playlistIndex) {
        store.set({
          playlistIndex: 0
        });
      }

      return 'PLAYING';
    },
    stop: 'IDLE'
  },
  'PAUSED': {
    onEnter: () => {
      const store = Store.get();
      Audio.pause();
      StoreEvents.emit('pause', store.currentAudio, store.playlistIndex);
    },
    play: 'PLAYING',
    stop: 'IDLE'
  }
});
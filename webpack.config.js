const webpack = require('webpack'); //to access built-in plugins
const autoprefixer = require('autoprefixer');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const externals = {
  react: 'react',
  'react-dom': 'react-dom',
  freezer: 'freezer-js',
  stately: 'stately.js',
  'react-input-slider': 'react-input-slider',
  'react-stickynode': 'react-stickynode',
};

const app = {
  name: 'app',
  devtool: 'none',
  entry: {
    'player.main': './src/jsx/index.jsx'
  },
  output: {
    path: path.resolve(__dirname, 'dist/js'),
    filename: "[name].js"
  },
  module: {
    rules: [
      {
        test: /\.jsx$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: [
            'es2016',
            'react',
            'stage-0',
          ],
          plugins: ['transform-react-jsx']
        }
      },
      {
        test: /\.css/,
        loader: 'style-loader!css-loader'
      },
      {
        test: /\.scss/,
        loader: 'style-loader!css-loader!resolve-url-loader!sass-loader?sourceMap'
      },
      {
        test: /\.(png|jpg|gif|woff|woff2)$/,
        loader: 'url-loader',
      }
    ]
  },
  resolve: {
    extensions: [".js", ".jsx", ".json"],
    modules: [
      __dirname,
      'node_modules',
    ],
    alias: externals
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.LoaderOptionsPlugin({
      options: {
        context: __dirname,
        postcss: [
          autoprefixer
        ]
      }
    }),
    new webpack.DllReferencePlugin({
      context: __dirname,
      manifest: path.resolve(__dirname, "src/dll/manifest.json"),
    }),
  ]
};

const styles = {
  name: 'styles',
  entry: {
    styles: ['./src/scss/input-slider.scss', './src/scss/react-sticky.scss', './src/scss/main.scss']
  },
  resolve: {
    extensions: [".scss"],
  },
  output: {
    path: path.resolve(__dirname, 'dist', 'css'),
    filename: "player.[name].css"
  },
  module: {
    rules: [
      {
        test: /\.scss/,
        use: ExtractTextPlugin.extract({
          use: [
            'css-loader',
            'resolve-url-loader',
            {
              loader: 'postcss-loader',
              options: {
                config: {
                  path: './postcss.config.js'
                }
              }
            },
            'sass-loader',
          ]
        })
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin('player.styles.css')
  ]
};

const vendors = {
  name: 'vendors',
  resolve: {
    extensions: [".js", ".jsx"],
    modules: [
      __dirname,
      'node_modules',
    ],
  },
  entry: {
    vendors: ['react', 'react-dom', 'freezer-js', 'stately.js', 'react-input-slider', 'react-stickynode', 'promise-polyfill', 'whatwg-fetch'],
  },
  output: {
    path: path.resolve(__dirname, 'dist', 'js'),
    filename: "player.[name].js",
    library: "[name]_[hash]"
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.DllPlugin({
      context: __dirname,
      name: "[name]_[hash]",
      path: path.join(__dirname, "src/dll/manifest.json"),
    })
  ]
};

module.exports = [vendors, styles, app];